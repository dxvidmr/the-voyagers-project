This website is the result of the digital edition project of the text *Relacion de un Viaje al Río de la Plata* in Acarete du Biscay.   
Editing was done by **The Voyagers!** during the course [Digital Publishing with minimal computing](https://mith.umd.edu/minimaldigipub/en.html),
organized by the University of Maryland (USA) and the University of Salvador (Argentina) between September and December 2020.

Visit the website [here](https://dxvidmr.gitlab.io/the-voyagers-project/)